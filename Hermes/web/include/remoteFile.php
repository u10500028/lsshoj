<?php
require_once("db_info.inc.php");

function push_file($srcFn, $dstFn) {
	global $FILE_USER, $FILE_HOST;
	$cmd = "scp \"$srcFn\" \"$FILE_USER@$FILE_HOST:$dstFn\"";
	#echo $cmd;
	exec(escapeshellcmd($cmd)." 2>&1", $outputs, $res);
	if ($res!=0) {
		error_log("Hermes: ".end($outputs)." ($cmd)");
		#print_r(end($outputs));
		return FALSE;
	}
	else {
		return TRUE;
	}
}

function pull_file($srcFn, $dstFn="") {
	global $FILE_USER, $FILE_HOST;
	if ($dstFn==FALSE||$dstFn=="") {
		$dstFn = tempnam("", "");
	}
	$cmd = "scp \"$FILE_USER@$FILE_HOST:$srcFn\" \"$dstFn\"";
	exec(escapeshellcmd($cmd)." 2>&1", $outputs, $res);
	if ($res==0) {
		return $dstFn;
	}
	else {
		error_log("Hermes: ".end($outputs)." ($cmd)");
		return FALSE;
	}
}

function remote_mkdir($dstDir) {
	global $FILE_USER, $FILE_HOST;
	$cmd = "ssh $FILE_USER@$FILE_HOST mkdir -p $dstDir";
	exec(escapeshellcmd($cmd)." 2>&1", $outputs, $res);
	if ($res==0) {
		return TRUE;
	}
	else {
		error_log("Hermes: ".end($outputs)." ($cmd)");
		return FALSE;
	}
}

function remote_unlink($dstFn) {
	global $FILE_USER, $FILE_HOST;
	$cmd = "ssh $FILE_USER@$FILE_HOST rm -f $dstFn";
	exec(escapeshellcmd($cmd)." 2>&1", $outputs, $res);
	if ($res==0) {
		return TRUE;
	}
	else {
		error_log("Hermes: ".end($outputs)." ($cmd)");
		return FALSE;
	}
}

function remote_rename($srcFn, $dstFn) {
	global $FILE_USER, $FILE_HOST;
	$cmd = "ssh $FILE_USER@$FILE_HOST mv $srcFn $dstFn";
	exec(escapeshellcmd($cmd)." 2>&1", $outputs, $res);
	if ($res==0) {
		return TRUE;
	}
	else {
		error_log("Hermes: ".end($outputs)." ($cmd)");
		return FALSE;
	}
}

function remote_listdir( $srcFn ) {
	global $FILE_USER, $FILE_HOST;
	$cmd = "ssh $FILE_USER@$FILE_HOST ls -1 $srcFn";
	exec(escapeshellcmd($cmd)." 2>&1", $outputs, $res);
	$newOut = array();
	if ($res==0) {
		foreach ($outputs as $elem) {
			$newOut[] = "$srcFn/$elem";
		}
		return $newOut;
	}
	else {
		error_log("Hermes: ".end($outputs)." ($cmd)");
		return FALSE;
	}
}
?>
