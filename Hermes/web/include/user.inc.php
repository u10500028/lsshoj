<?php
class User {
	public $user_id;
	public $nick, $email, $school;
	public $defunct;
	public $privilege;
	public $plnPasswd;
	public $cyphPasswd;

	static function getUserList() {
		$UList = array();
		$sql = "
SELECT U.user_id, U.nick, U.email, U.school, U.defunct, 
 A.rightstr AS administrator, S.rightstr AS source_browser, C.rightstr AS contest_creator, 
 H.rightstr AS http_judge, I.rightstr AS invisible
FROM `users` AS U
 LEFT JOIN privilege AS A ON U.user_id = A.user_id AND A.rightstr = 'administrator'
 LEFT JOIN privilege AS S ON U.user_id = S.user_id AND S.rightstr = 'source_browser'
 LEFT JOIN privilege AS C ON U.user_id = C.user_id AND C.rightstr = 'contest_creator'
 LEFT JOIN privilege AS H ON U.user_id = H.user_id AND H.rightstr = 'http_judge'
 LEFT JOIN privilege AS I ON U.user_id = I.user_id AND I.rightstr = 'invisible'
";
		$result=mysql_query($sql) or die(mysql_error());
		while ($row=mysql_fetch_assoc($result)){
			$privs = array($row['administrator'],$row['source_browser'],$row['contest_creator'],$row['http_judge'],$row['invisible']);
			$UList[] = new User($row['user_id'], $row['nick'], $row['school'], $row['email'], $row['defunct'], $privs);
		}
		mysql_free_result($result);

		return $UList;
	}

	function __construct($uid) {
		if (func_num_args()==1) {
			$uid = mysql_real_escape_string($uid);
			$sql = "SELECT * FROM `users` WHERE user_id='$uid'";
			$result = mysql_query($sql);
			if ($row=mysql_fetch_object($result)) {
				$this->user_id = $row->user_id;
				$this->nick = $row->nick;
				$this->school = $row->school;
				$this->email = $row->email;
				$this->defunct = $row->defunct;
				$this->cyphPasswd = $row->password;
			}
			mysql_free_result($result);

			$sql = "SELECT rightstr FROM privilege WHERE user_id='$this->user_id'";
			$result = mysql_query($sql);
			$this->privilege = array();
			while ($row=mysql_fetch_object($result)) {
				$this->privilege[] = $row->rightstr;
			}
			mysql_free_result($result);
		}
		else {
			list($this->user_id, $this->nick, $this->school, $this->email, $this->defunct, $this->privilege) = func_get_args();
		}
	}

	function updateDB() {
		$err_str = "";
		$err_cnt = 0;

		$len=strlen($this->nick);
		if ($len>100){
			$err_str=$err_str."Nick Name Too Long!\n";
			$err_cnt++;
		}else if ($len==0) $this->nick=$this->user_id;
		$len=strlen($this->plnPasswd);
		if ($len<6 && $len>0){
			$err_cnt++;
			$err_str=$err_str."Password should be Longer than 6!\n";
		}
		$len=strlen($this->school);
		if ($len>100){
			$err_str=$err_str."School Name Too Long!\n";
			$err_cnt++;
		}
		$len=strlen($this->email);
		if ($len>100){
			$err_str=$err_str."Email Too Long!\n";
			$err_cnt++;
		}
		if ($err_cnt>0){
			$retv = "";
			$retv .= "<script language='javascript'>\n";
			$retv .= "alert('";
			$retv .= $err_str;
			$retv .= "');\n history.go(-1);\n</script>";
			return $retv;
		}

		if (strlen($this->plnPasswd)==0) 
			$password=$this->cyphPasswd;
		else 
			$password=MD5($this->plnPasswd);
		$uid=mysql_real_escape_string($this->user_id);
		$nick=mysql_real_escape_string(htmlspecialchars ($this->nick));
		$school=mysql_real_escape_string(htmlspecialchars ($this->school));
		$email=mysql_real_escape_string(htmlspecialchars ($this->email));
		$defunct=mysql_real_escape_string(htmlspecialchars ($this->defunct));
		$sql="UPDATE `users` SET";
		if ($password!="")
			$sql.="`password`='".($password)."',";
		$sql.="`nick`='".($nick)."',"
		."`school`='".($school)."',"
		."`email`='".($email)."',"
		."`defunct`='".($defunct)."' "
		."WHERE `user_id`='".$uid."'"
		;
		//echo $sql;
		//exit(0);
		mysql_query($sql) or die("Update `users` Error!\n");
		//
		$sql = "DELETE FROM `privilege` WHERE `user_id`='$uid'";
		mysql_query($sql) or die("Update `privilege`1 Error!\n");

		if (count($this->privilege)>0) {
			$sql = "INSERT INTO `privilege` (`user_id`, `rightstr`, `defunct`) VALUES";
			foreach ($this->privilege as $i=>$rightstr) {
				if ($i==0) $sql.=" ";
				else       $sql.=",";
				$rightstr=mysql_real_escape_string($rightstr);
				$sql .= "('$uid', '$rightstr', 'N')";
			}
			mysql_query($sql) or die("Update `privilege`2 Error!\n");
		}
		return "update $uid OK!";
	}
}
?>
