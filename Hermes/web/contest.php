<?php
$OJ_CACHE_SHARE=true;
require_once("./include/db_info.inc.php");
?>
<?php require_once("./include/const.inc.php")?>
<?php 
require_once("./include/contest.inc.php");
function contestCmp($a,$b) {
	if ($a->running()&&!$b->running()) return -1;
	if ($b->running()&&!$a->running()) return 1;
	if ($a->notstart()&&!$b->notstart()) return -1;
	if ($b->notstart()&&!$a->notstart()) return 1;
	if ($a->end_time<$b->end_time) return -1;
	if ($b->end_time<$a->end_time) return 1;
	return 0;
}

if (isset($_GET['cid'])){
	$cid=intval($_GET['cid']);

	require_once("contest-header.php");

	$Contest = Contest::getContest($cid);
	// check contest valid
	if ($Contest==NULL) {
		echo "<h2>No Such Contest!</h2>";
		require_once("oj-footer.php");
		exit(0);
	}
?>
<center>
<h3><?php echo $Contest->title;?></h3>
</center>
<center>
<div id="contest_info">
<font color=#993399><?php echo $Contest->start_time;?></font> ~ <font color=#993399><?php echo $Contest->end_time;?></font><?php if ($Contest->duration!='0') echo ", 長度: $Contest->duration mins";?><br>
<?php
	$now=time();
	if ($Contest->ended()) $status1 = "<font color=red>已結束</font>";
	else if ($Contest->notstart()) $status1 = "<font color=red>還沒開始</font>";
	else $status1 = "<font color=red>舉行中</font>";
	if ($Contest->private=='0') $status2 = "<font color=blue>公開</font>";
	else $status2 = "<font color=red>不公開</font>"; 
	echo " 狀況: $status1&nbsp;&nbsp;$status2";

	if ($Contest->penalty>0) $penalty = "<font color=red>$Contest->penalty</font>";
	else $penalty = "<font color=green>$Contest->penalty</font>";
	echo " Penalty: $penalty\n";
?>
<br />
現在時間: <font color=#993399><span id=nowdate ><?php echo date("Y-m-d H:i:s");?></span></font>
</div>
<div id="countdown" style="display: none;">
<br/><br />
<h2>距離開始的時間</h2>
<h1 id="countdown-time"></h1>
<script type="text/Javascript">
function FormatNumberLength(num, length) {
    var r = "" + num;
    while (r.length < length) {
        r = "0" + r;
    }
    return r;
}
/** CountDown Timer**/
var remain_time = <?php echo strtotime($Contest->start_time)-time();?>;
if(remain_time > 0){
    document.getElementById("countdown").style.display = "inline";
    setInterval(function() {
        if(remain_time == 0) window.location.reload(1);
        var remain_seconds = Math.floor(remain_time %60);
        var remain_minutes = Math.floor(remain_time /60 %60);
        var remain_hours = Math.floor(remain_time /60 /60 %24);
        //console.log(remain_hours);
        document.getElementById("countdown-time").innerHTML = FormatNumberLength( remain_hours , 2 )+":"+ FormatNumberLength(remain_minutes , 2 ) +":"+ FormatNumberLength( remain_seconds, 2);
        remain_time--;
    }, 1000);
}
</script>
</div>
</center>
<?php
	if (!isset($_SESSION['administrator']) && $now<strtotime($Contest->start_time)){
		require_once("oj-footer.php");
		exit(0);
	}
	
	if (!$contest_ok){
		echo "<br><h1>Not Invited!</h1>";
		require_once("oj-footer.php");
		exit(1);
	}


	$duration = intval($Contest->duration) ;

	if ($duration!=0&&!$Contest->ended()&&!isset($_SESSION['user_id'])) {
		echo "<br><h1>Login First!</h1>" ;
		require_once("oj-footer.php") ;
		exit(1) ;
	}

	if($duration!=0&&!$Contest->ended()&&isset($_SESSION['user_id']))
	{
		$Contest->setUser($_SESSION['user_id']);

		if (!$Contest->user->user_start) {
			if(isset($_GET['start']))
			{
				$Contest->user->startContest();
			}
			else // isset $_GET['start']
			{
				if($Contest->running())
				{
?>
<br />
<br />
<center>
	<form method=GET action='<?php echo $_SERVER['PHP_SELF']?>'>
		<input type=hidden name=cid value=<?php echo $cid?>>
		<input type=hidden name=start value=start>
		Contest duration: <?php echo $Contest->duration;?> minutes, click <input type=submit value=here name=submit> to start your contest
	</form>
</center>
<?php
				}
				exit(0) ;
			}
		} // !user_start
		$end_time = $Contest->user->user_end_time();
	} // duration!=0
	else {
		$end_time = strtotime($Contest->end_time);
	}
?>
<br>
<script>
var user_end_time = <?php echo $end_time;?> * 1000 ;
</script>
<?php
	$probList = $Contest->getProblemList();	
?>
<center>
<table width=60% class="pure-table">
<thead>
	<tr><td width=5><td width=34%><b>Problem ID</b><td width=65%><b>Title</b></tr>
</thead>
<tbody>
<?php
	foreach ($probList as $cnt=>$problem) {
		if ($cnt&1) echo "\t<tr class='pure-table-odd' >\n";
		else        echo "\t<tr>\n";
?>
<?php
		if ($Contest->duration!=0&&!$Contest->ended()) {
			if ($problem->check_try()) echo "\t<td><font color=red>?</font></td>\n";
			else                       echo "\t<td></td>\n";
		}
		else {
			if ($problem->check_ac())       echo "\t<td><i class=\"icono-check\" style=\"color: #72962e;\"></i></td>\n";
			else if ($problem->check_try()) echo "\t<td><i class=\"icono-cross\" style=\"color: #FF0000;\"></i></td>\n";
			else                            echo "\t<td></td>\n";
		}
?>
		<td><?php echo $problem->pid;?> Problem <?php echo $PID[$problem->cpid];?></td>
		<td><a href='problem.php?cid=<?php echo $cid;?>&pid=<?php echo $problem->cpid;?>'><?php echo $problem->title;?></a></td>
<?php
		echo "\t</tr>\n";
	}
?>
</tbody>
</table>
<!--
可能這裡空的部份改成競賽通知
<br>
<a class="pure-button button-primary" href='status.php?type=contest&cid=<?php echo $cid;?>'>狀態</a>&nbsp;
<a class="pure-button button-primary" href='contestrank.php?cid=<?php echo $cid;?>'>名次</a>
<a class="pure-button button-primary" href='conteststatistics.php?cid=<?php echo $cid;?>'>統計</a>
-->
</center>
<?php
}


else { // isset $_GET['cid']
	require_once("oj-header.php");

	$CList = Contest::getContestList();
	usort($CList, "contestCmp");
?>
<center>
	<h2>競賽列表</h2>伺服器時間:<span id=nowdate></span>
<table class="pure-table pure-table-bordered">

<thead>
<tr align=center><td width=7%>狀態</td><td width=7%>ID</td><td width=37%>競賽名稱</td><td width=20%>開始時間</td><td width=18%>類型</td><td width=7%>長度</td></tr>
</thead>
<tbody>
<?php
foreach ($CList as $cnt=>$C) {
	if ($cnt&1) echo "<tr align=center class=oddrow>";
	else        echo "<tr align=center class=evenrow>";

	// past
	if ($C->ended())         echo "<td><font color=black>已結束</font></td>";
	// pending
	else if ($C->notstart()) echo "<td><font color=green>即將到來</font></td>";
	// running
	else                     echo "<td><font color=red>舉行中</font></td>";

	echo "<td>$C->cid</td>";

	if ($C->private=='1') {
		if (isset($_SESSION['c'.$cid]) || isset($_SESSION['administrator'])) {
			echo "<td><a href='contest.php?cid=$C->cid'>$C->title</a> <small>(invited)</small></td>";
		}
		else {
			echo "<td>$C->title <small>(private)</small></td>";
		}
	}
	else {
			echo "<td><a href='contest.php?cid=$C->cid'>$C->title</a></td>";
	}

	// past
	if ($C->ended())         echo "<td><font color=black>~$C->end_time</font></td>";
	// pending
	else if ($C->notstart()) echo "<td><font color=green>$C->start_time~</font></td>";
	// running
	else                     echo "<td><font color=red>~$C->end_time</font></td>";

	if ($C->ranked=='1') $rankStr=', <a href="contesttotalrank.php">(R)</a>';
	else                 $rankStr='';
	if ($C->duration=='0') echo "<td>realtime$rankStr</td>" ;
	else                   echo "<td>asynchronous$rankStr</td>" ;

	if ($C->duration=='0') echo "<td>".((strtotime($C->end_time)-strtotime($C->start_time))/60)." mins</td>" ;
	else                   echo "<td>$C->duration mins</td>" ;

	echo "</tr>";
}
?>
</tbody></table></center>
<?php
} // else: isset $_GET['cid']
?>
<script>
var diff=new Date("<?php echo date("Y/m/d H:i:s")?>").getTime()-new Date().getTime();
//alert(diff);
function clock()
    {
      var x,h,m,s,n,xingqi,y,mon,d;
      var x = new Date(new Date().getTime()+diff);
      y = x.getYear()+1900;
      if (y>3000) y-=1900;
      mon = x.getMonth()+1;
      d = x.getDate();
      xingqi = x.getDay();
      h=x.getHours();
      m=x.getMinutes();
      s=x.getSeconds();
  
      n=y+"-"+mon+"-"+d+" "+(h>=10?h:"0"+h)+":"+(m>=10?m:"0"+m)+":"+(s>=10?s:"0"+s);
      //alert(n);
      document.getElementById('nowdate').innerHTML=n;

      var count_down = Math.floor((user_end_time-x.getTime())/1000) ;
      if(count_down<0) count_down = 0 ;
      h = Math.floor(count_down/60/60) ;
      m = Math.floor(count_down/60)%60 ;
      s = count_down%60 ;
      n = h+":"+(m>=10?m:"0"+m)+":"+(s>=10?s:"0"+s);
      if(document.getElementById('count_down')!=null) document.getElementById('count_down').innerHTML=n ;

      setTimeout("clock()",1000);
    } 
    clock();
</script>
<?php require_once("oj-footer.php");?>
