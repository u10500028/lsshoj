<?php
require_once("config.php");
	require_once('./include/cache_start.php');
    
    function checkcontest(){
		require_once("./include/db_info.inc.php");
		$sql="SELECT count(*) FROM `contest` WHERE `end_time`>NOW() AND `defunct`='N'";
		$result=mysql_query($sql);
		$row=mysql_fetch_row($result);
		if (intval($row[0])==0) $retmsg="競賽";
		else $retmsg="<span class=red>".$row[0]."&nbsp;競賽</span>";
		mysql_free_result($result);
		return $retmsg;
	}
	if($OJ_ONLINE){
		require_once('./include/online.php');
		$on = new online();
	}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LSSH Online Judge</title>
    
    <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/pure-min.css" integrity="sha384-nn4HPE8lTHyVtfCBi5yW9d20FjT8BJwUXyWZT9InLYax14RDjBj46LmSztkmNP9w" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.0/build/grids-responsive-min.css">
    <link rel="stylesheet" href="http://icono-49d6.kxcdn.com/icono.min.css">
	
    <link rel="stylesheet" href=<?php echo $OJ_BASE;?>include/menu.css>
    <link rel="stylesheet" href=<?php echo $OJ_BASE;?>include/theme.css>

</head>

<body>
	
	
<div class="menu-wrapper pure-g" id="menu">
	<?php 
		$url=basename($_SERVER['REQUEST_URI']);
		//echo $url;
	?>
    <div class="pure-u-1 pure-u-md-1-3">
        <div class="pure-menu">
            <a href=<?php echo $OJ_BASE;?> class="pure-menu-heading menu-brand menu-text">LSSHOJ</a>
            <a href="#" class="menu-toggle" id="toggle"><s class="bar"></s><s class="bar"></s></a>
        </div>
    </div>
    <div class="pure-u-1 pure-u-md-1-3">
        <div class="pure-menu pure-menu-horizontal menu-can-transform">
            <ul class="pure-menu-list">
                <li class="pure-menu-item <?php if ($url==basename(__DIR__) ) echo "menu-selected";?>"><a href=<?php echo $OJ_BASE;?> class="pure-menu-link menu-text">首頁</a></li>
                <li class="pure-menu-item <?php if ($url=="contest.php") echo "menu-selected";?>"><a href=<?php echo $OJ_BASE;?>contest.php class="pure-menu-link menu-text">競賽</a></li>
                <li class="pure-menu-item <?php if ($url=="problemset.php") echo "menu-selected";?>"><a href=<?php echo $OJ_BASE;?>problemset.php class="pure-menu-link menu-text">題庫</a></li>
                <li class="pure-menu-item <?php if ($url=="status.php?type=problem") echo "menu-selected";?>"><a href=<?php echo $OJ_BASE;?>status.php?type=problem class="pure-menu-link menu-text">狀態</a></li>
                <li class="pure-menu-item <?php if ($url=="ranklist.php") echo "menu-selected";?>"><a href=<?php echo $OJ_BASE;?>ranklist.php class="pure-menu-link menu-text">排名</a></li>
                <li class="pure-menu-item <?php if ($url=="faqs.php") echo "menu-selected";?>"><a href=<?php echo $OJ_BASE;?>faqs.php class="pure-menu-link menu-text">常見問答</a></li>
            </ul>
        </div>
    </div>
    <div class="pure-u-1 pure-u-md-1-3">
        <div class="pure-menu pure-menu-horizontal menu-menu-3 menu-can-transform">
            <ul class="pure-menu-list" id=profile>
				<?php
					/**
					Move Profile Bar to here
					Original : include/Profile.php
					Original code : <script src = " include/Profile.php"></script>
					**/
					//Proflile Bar
				
				    function checkmail(){
						$sql="SELECT count(1) FROM `mail` WHERE 
								new_mail=1 AND `to_user`='".$_SESSION['user_id']."'";
						$result=mysql_query($sql);
						if(!$result) return false;
						$row=mysql_fetch_row($result);
						$retmsg="<span id=red>(".$row[0].")</span>";
						mysql_free_result($result);
						return $retmsg;
					}
					if (isset($_SESSION['user_id'])){
						$sid=$_SESSION['user_id'];
						echo '<li class="pure-menu-item '. (explode("?", $url)[0] =="profile.php" ? "menu-selected": "").'"><a href="'.$OJ_BASE.'user/profile.php?user='.$sid.'" class="pure-menu-link menu-text"><i class="icono-user"></i></a></li>';
						echo '<li class="pure-menu-item '. (explode("?", $url)[0] =="mail.php" ? "menu-selected": "").'"><a href="'.$OJ_BASE.'user/mail.php class="pure-menu-link menu-text"><i class="icono-comment"></i></a></li>';
						echo '<li class="pure-menu-item '. ($url =="setting.php" ? "menu-selected": "").'"><a href="'.$OJ_BASE.'user/setting.php" class="pure-menu-link menu-text"><i class="icono-gear"></i></a></li>';
						echo '<li class="pure-menu-item '. ($url=="logout.php" ? "menu-selected": "").'"><a href='.$OJ_BASE.'user/logout.php class="pure-menu-link menu-text">登出</a></li>';
					}else{
						echo '<li class="pure-menu-item '. ($url=="loginpage.php" ? "menu-selected": "").'"><a href='.$OJ_BASE.'user/loginpage.php class="pure-menu-link menu-text">登入</a></li>';
						echo '<li class="pure-menu-item '. ($url=="registerpage.php" ? "menu-selected": "").'"><a href='.$OJ_BASE.'user/registerpage.php class="pure-menu-link menu-text">註冊</a></li>';
					}
					if (isset($_SESSION['administrator'])||isset($_SESSION['contest_creator'])){
						echo '<li class="pure-menu-item"><a href='.$OJ_BASE.'admin class="pure-menu-link menu-text">管理</a></li>';
					}
				?>
            </ul>
        </div>
    </div>
</div>
<script type='text/javascript' src="<?php echo $OJ_BASE;?>include/menu.js"></script>
<!--end menu-->
<div id="main" class="main">
