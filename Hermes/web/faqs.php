<?php
$cache_time=3600;
$OJ_CACHE_SHARE=false;
	require_once('oj-header.php'); 
	require_once("./include/db_info.inc.php");
?>
<center>
  <h1>LSSHOJ Online Judge FAQ</h1>
</center>

<section>
	
<p>
	<font color=green>Q</font>:LSSHOJ 是用什麼編譯器，編譯器的設定是什麼?<br>
	<font color=red>A</font>:LSSHOJ是跑在<a href="http://www.debian.org/">Debian Linux</a>上的.我們是使用<a href="http://gcc.gnu.org/">GNU GCC/G++</a> 來編譯C/C++, <a href="http://www.freepascal.org">Free Pascal</a> 來編譯Pascal以及 <a href="http://www.oracle.com/technetwork/java/index.html">sun-java-jdk1.6</a>來編譯Java。 編譯器的設定是:<br>
</p>
<table class="pure-table pure-table-bordered">
  <tr>
    <td>C:</td>
    <td><font color=#DF7401>gcc Main.c -o Main -O2 -Wall -lm --static -std=c99 -DONLINE_JUDGE</font></td>
  </tr>
  <tr>
    <td>C++:</td>
    <td><font color=#DF7401>g++ Main.cc -o Main -O2 -Wall -lm --static -DONLINE_JUDGE</font></td>
  </tr>
  <tr>
    <td>Pascal:</td>
    <td><font color=#DF7401>fpc Main.pas -oMain -O1 -Co -Cr -Ct -Ci </font></td>
  </tr>
  <tr>
    <td>Java:</td>
    <td><font color="#DF7401">javac -J-Xms32m -J-Xmx256m Main.java</font>
    <br>
    <font size="-1" color="red">*Java 有多2秒以及多512M記憶體當在評測執行的時候。</font>
    </td>
  </tr>
</table>
<p> 我們的編譯器版本:<br>
  <font color=#DF7401>gcc (Ubuntu/Linaro 4.4.4-14ubuntu5) 4.4.5</font><br>
  <font color=#DF7401>glibc 2.3.6</font><br>
<font color=#DF7401>Free Pascal Compiler version 2.4.0-2 [2010/03/06] for i386<br>
java version "1.6.0_22"<br>
</font></p>

</section>
<section>
	
<font color=green>Q</font>:輸入輸出是什麼?<br>
<font color=red>A</font>:你的程式應該要從stdin('Standard Input')讀取輸入以及將輸出寫到 stdout('Standard Output').例如,你可以使用在C裡使用'scanf'或是在C++裡使用'cin'來從stdin讀取輸入,然後使用在C裡使用'printf'或是在C++裡使用'cout'來輸入寫到stdout.<br>
使用者的程式是不可以讀寫檔案的, 你會得到"<font color=green>執行錯誤</font>"如果你試著嘗試.<br><br>
這是C++的範例程式:<br>
<link href='highlight/styles/shCore.css' rel='stylesheet' type='text/css'/> 
<link href='highlight/styles/shThemeDefault.css' rel='stylesheet' type='text/css'/> 
<script src='highlight/scripts/shCore.js' type='text/javascript'></script> 
<script src='highlight/scripts/shBrushCpp.js' type='text/javascript'></script> 
<script src='highlight/scripts/shBrushCss.js' type='text/javascript'></script> 
<script src='highlight/scripts/shBrushJava.js' type='text/javascript'></script> 
<script src='highlight/scripts/shBrushDelphi.js' type='text/javascript'></script> 
<script src='highlight/scripts/shBrushRuby.js' type='text/javascript'></script> 
<script src='highlight/scripts/shBrushBash.js' type='text/javascript'></script>
<script src='highlight/scripts/shBrushPython.js' type='text/javascript'></script> 
<script src='highlight/scripts/shBrushPhp.js' type='text/javascript'></script> 
<script src='highlight/scripts/shBrushPerl.js' type='text/javascript'></script> 
<script src='highlight/scripts/shBrushCSharp.js' type='text/javascript'></script>
<script language='javascript'> 
SyntaxHighlighter.config.bloggerMode = true;
SyntaxHighlighter.config.clipboardSwf = 'highlight/scripts/clipboard.swf';
SyntaxHighlighter.all();
</script>

<pre class="brush:c++;">
#include &lt;iostream&gt;
using namespace std;
int main(){
    int a,b;
    while(cin >> a >> b)
        cout << a+b << endl;
	return 0;
}
</pre>
這是C的範例程式:<br>
<pre class="brush:c;">
#include &lt;stdio.h&gt;
int main(){
    int a,b;
    while(scanf("%d %d",&amp;a, &amp;b) != EOF)
        printf("%d\n",a+b);
	return 0;
}
</pre>
這是PASCAL的範例程式:<br>
<pre class="brush:pascal;">
program p1001(Input,Output); 
var 
  a,b:Integer; 
begin 
   while not eof(Input) do 
     begin 
       Readln(a,b); 
       Writeln(a+b); 
     end; 
end.
</pre>
<br><br>

這是Java的範例程式 :<br>
<pre class="brush:java;">
import java.util.*;
public class Main{
	public static void main(String args[]){
		Scanner cin = new Scanner(System.in);
		int a, b;
		while (cin.hasNext()){
			a = cin.nextInt(); b = cin.nextInt();
			System.out.println(a + b);
		}
	}
}
</pre>


</section>
<section>

<font color=green>Q</font>:為什麼我會拿到一個編譯錯誤? 他明明很好!<br>
<font color=red>A</font>:GNU以及MS-VC++有些不同,像是:<br>
<ul>
  <li><font color=#DF7401>main</font> 必須以<font color=#DF7401>int</font>宣告, <font color=#DF7401>void main</font>的結果會是編譯錯誤.<br> 
  <li><font color=green>i</font> 超數出了宣告區塊 "<font color=#DF7401>for</font>(<font color=#DF7401>int</font> <font color=green>i</font>=0...){...}"<br>
  <li><font color=green>itoa</font> 不是ANSI函式.<br>
  <li><font color=green>__int64</font> 不是ANSI, 但是64-bit整數你可以使用<font color=#DF7401>long long</font>.<br>試著使用#define __int64 long long 如果是使用MSVC6.0
</ul>

</section>
<section>

<font color=green>Q</font>:LSSHOJ的回覆XXXXX是什麼意思?<br>
<font color=red>A</font>:這裡是所有可能的回覆以及他的意思:<br>
<p><font color=#DF7401>等待中</font> : LSSHOJ有點忙，請你等他一下，他會盡快幫你處裡喔~</p>
<p><font color=#DF7401>等待重判</font>: 測試資料已經更新, LSSHOJ正在重新評測.</p>
<p><font color=#DF7401>編譯中</font> : LSSHOJ在編譯你的程式碼.<br>
</p>
<p><font color="#DF7401">執行並評判</font>: 你的程式正在LSSHOJ執行且評測中.<br>
  </p>
<p><font color=#DF7401>正確</font> : 完美~~ 你的程式是對的!.<br>
  <br>
  <font color=#DF7401>格式錯誤</font> : 你的輸出不太對,但是你有可能是對的.檢查你的輸出是否有空格,空行......等與題目不一樣的.<br>
  <br>
  <font color=#DF7401>答案錯誤</font> : 你的程式就是錯了啊，就是有BUG啊~~~ ;-).<br>
  <br>
  <font color=#DF7401>時間超限</font> : 你的程式跑太久了啦~~<br>
  <br>
  <font color=#DF7401>記憶體超限</font> : 你的程式吃太記憶體了啦~~ <br>
  <br>
  <font color=#DF7401>輸出超限</font>: 你的程式吐太多東西出來了啦~~. <br>
  <br>
  <font color=#DF7401>執行錯誤</font> : 其他程式執行期間的錯誤, 像是'記憶體區段錯誤','floating point exception','使用不存在的函式', '使用不存在的記憶體'.....等等.<br>
</p>
<p>  <font color=#DF7401>編譯錯誤</font> : 編譯器(gcc/g++/gpc) 不能編譯你的ANSI程式. 當然, 警告訊息不是錯誤訊息. 點選LSSHOJ回覆的連結來檢視錯誤訊息.<br>
  <br>
</p>

</section>
<section>

<font color=green>Q</font>:如何參加線上競賽?<br>
<font color=red>A</font>:你可以送出任何在LSSHOJ題庫裡的題目嘛? 如果你可以, 這會是參加線上競賽的帳號.
如果你不行, 那請你先<a href=registerpage.php>註冊</a>一個帳號.<br>
<br>

</section>
<section>

<center>
  <h1>任何LSSHOJ的問題/建議,請與<a href="http://lssc.cf/">麗山星創</a>聯絡,或是直接與麗山的電腦老師聯絡就好了喔~</h1>
</center>

</section>
<!--R1280-->
<?php require_once('oj-footer.php');?>
