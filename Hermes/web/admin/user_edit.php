<?php require("admin-header.php");
require_once("../include/set_get_key.php");
require_once("../include/user.inc.php");
if (!(isset($_SESSION['administrator']))){
	echo "<a href='../loginpage.php'>Please Login First!</a>";
	exit(1);
}

if (!array_key_exists('user_id', $_POST)||!array_key_exists('submit', $_POST)) {
	$uid=$_GET['user'];
	$U = new User($uid);
?>
<title>Edit User</title>
<center><h2>Edit User</h2></center>
<form action="user_edit.php" method="post">
	<center><table>
		<tr><td width=25%>User ID:</td>
			<td width=75%><?=$U->user_id?><input name="user_id" size=20 type=hidden value="<?=$U->user_id?>"></td>
		</tr>
		<tr><td>defunct:</td>
<?php
		$defunctChk = array("Y"=>"", "N"=>"");
		$defunctChk[$U->defunct] = "checked";
?>
			<td><input type=radio name="defunct" value="Y" <?=$defunctChk['Y']?>>Y</input>
			    <input type=radio name="defunct" value="N" <?=$defunctChk['N']?>>N</input></td>
		</tr>
		<tr><td>nick:</td>
			<td><input name="nick" size=50 type=text value="<?=$U->nick?>"></td>
		</tr>
		<tr><td>reset new password:</td>
			<td><input name="password" size=20 type=password>*</td>
		</tr>
		<tr><td>repeat new password:</td>
			<td><input name="rptpassword" size=20 type=password>*</td>
		</tr>
		<tr><td>school:</td>
			<td><input name="school" size=30 type=text value="<?=$U->school?>"></td>
		</tr>
		<tr><td>email:</td>
			<td><input name="email" size=30 type=text value="<?=$U->email?>"></td>
		</tr>
		<tr><td>privilege</td><td>
<?php
	$privLong = array('administrator', 'source_browser', 'contest_creator', 'http_judge', 'invisible');
	$privShort = array('A', 'S', 'C', 'H', 'I');
	foreach ($privLong as $i=>$long) {
		$short = $privShort[$i];
		if (in_array($long, $U->privilege)) $checked = "checked";
		else $checked = "";
		echo "<input type=checkbox name=privilege[] value=$long id=priv$short $checked><label for=priv$short>$long</label>";
	}
?>			
		</td></tr>
		<tr><td></td>
			<td><input value="Submit" name="submit" type="submit">
				&nbsp; &nbsp;
				<input value="Reset" name="reset" type="reset"></td>
		</tr>
	</table></center>
	<br><br>
</form>
<?php
}
else {
	$uid=$_POST['user_id'];
	$U = new User($uid);
	if (!$U->user_id) {
		echo "ERROR: user id $uid not found!";
		return;
	}
	if ($_POST['password']!=$_POST['rptpassword']) {
		echo "ERROR: repeat password not match!";
		return;
	}

	$U->nick = $_POST['nick'];
	$U->plnPasswd = $_POST['password'];
	$U->school = $_POST['school'];
	$U->email = $_POST['email'];
	$U->defunct = $_POST['defunct'];
	$U->privilege = $_POST['privilege'];

	echo $U->updateDB();	
}
?>
