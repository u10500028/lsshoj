<?php
$cache_time=1;
chdir("..");
require_once("oj-header.php");


if (isset($_SESSION['user_id'])){
	echo "<a href=logout.php>Please logout First!</a>";
	exit(1);
}
?>
<center>
<form action=login.php method=post class="pure-form pure-form-stacked">
    <fieldset>
        <legend><h2>Login LSSHOJ</h2></legend>

        <label for="user_id">帳號（學號）:</label>
        <input name="user_id" id="user_id" type="text" size=20 required>

        <label for="password">密碼</label>
        <input name="password" id="password" type="password" size=20 required>

        <label for="vcode">驗證碼</label>
        <input name="vcode" id="vcode" type="text" size=4 required>
        <img src="vcode.php">
	
	<input name="submit" type="submit" class="pure-button button-primary" value="登入">
    </fieldset>
</form>
<form class="pure-form pure-form-stacked">
    <fieldset>
		<legend><h2>或是使用以下的網站登入</h2></legend>
		<a href="./oauth/Google.php" class="pure-button button-primary" >Google 登入</a>&nbsp;
    </fieldset>
</form>
</center>

<?php require_once("oj-footer.php");?>
