<?php 
chdir("..");
require_once('oj-header.php');?>
<?php if (!isset($_SESSION['user_id'])){
	echo "<a href=./loginpage.php>Please LogIn First!</a>";
	require_once('oj-footer.php');
	exit();
}
require_once('./include/db_info.inc.php');
$sql="SELECT * FROM `users` WHERE `user_id`='".$_SESSION['user_id']."'";
$result=mysql_query($sql);
$row=mysql_fetch_object($result);
?>

<center>
<form action="modify.php" class="pure-form pure-form-aligned" method="post">
	<legend>更新個人資訊</legend>
	<fieldset>
		<div class="pure-control-group">
			<label for="userid">User ID:</label>
			<input id="userid" type="text" value="<?php echo $_SESSION['user_id']?>" disabled>
			<?php require_once('./include/set_post_key.php');?>
		</div>
		<div class="pure-control-group">
			<label for="nick">Nick Name:</label>
			<input id="nick" name="nick" type=text value="<?php echo htmlspecialchars($row->nick)?>" >
		</div>
		<div class="pure-control-group">
			<label for="school">School:</label>
			<input id="school" name="school" type=text value="<?php echo htmlspecialchars($row->school)?>" >
		</div>
		<div class="pure-control-group">
			<label for="email">Email:</label>
			<input id="email" name="email" type=text value="<?php echo htmlspecialchars($row->email)?>" >
		</div>
	</fieldset>
	<fieldset>
		<legend>登入方式</legend>
		<div class="pure-control-group">
			<label for="option-LSSHOJ" class="pure-radio" >
		        <input id="option-LSSHOJ" type="radio" name="optionsRadios" value="LSSHOJ">
		        LSSHOJ
		    </label>
		    <label for="option-Google" class="pure-radio">
		        <input id="option-Google" type="radio" name="optionsRadios" value="Google">
		        Google
		    </label>
		</div>

		<div id="LSSHOJ_Auth">
			<div class="pure-control-group">
				<label for="opassword">Old Password:</label>
				<input id="opassword" name="opassword" type=password>
			</div>
			<div class="pure-control-group">
				<label for="npassword">New Password:</label>
				<input id="npassword" name="npassword" type=password>
			</div>
			<div class="pure-control-group">
				<label for="rptpassword">Repeat New Password::</label>
				<input id="rptpassword" name="rptpassword" type=password>
			</div>
		</div>
		<div id="Google_Auth">
			<?php
				if($row->oauth == 1) echo '目前連結到的帳號:'.$row->oauth_account;
				else echo '<a href="./oauth/Google.php" class="pure-button button-primary">連結到Google帳號</a>';
			?>
		</div>
	</fieldset>

	<fieldset>
		<div class="pure-controls">
			<input value="Submit" class="pure-button button-primary" name="submit" type="submit">
			<input value="Reset" class="pure-button button-primary" name="reset" type="reset">
		</div>
	</fieldset>
</form>
<script>
	document.getElementById("option-LSSHOJ").onclick = function() {
		document.getElementById("Google_Auth").style.display = 'none';
		document.getElementById("LSSHOJ_Auth").style.display = 'block';
    };
    document.getElementById("option-Google").onclick = function() {
		document.getElementById("Google_Auth").style.display = 'block';
		document.getElementById("LSSHOJ_Auth").style.display = 'none';
    };
    <?php
    	if($row->oauth == 1) echo 'document.getElementById("option-Google").click();';
    	else echo 'document.getElementById("option-LSSHOJ").click();';
    ?>
</script>
	<br>
	<a class="pure-button" href=export_ac_code.php>Download All AC Source</a>
</center>
<?php mysql_free_result($result);
?>
<?php require_once('oj-footer.php');?>
