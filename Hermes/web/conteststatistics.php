<?php
	$OJ_CACHE_SHARE=true;
	$cache_time=30;
		?><?php require_once("./include/db_info.inc.php");
require_once("./include/const.inc.php");
require_once("./include/my_func.inc.php");

require_once("contest-header.php");
// contest start time
if (!isset($_GET['cid'])) die("No Such Contest!");
$cid=intval($_GET['cid']);

if (!$contest_ok){
	echo "<br><h1>Not Invited!</h1>";
	require_once("oj-footer.php");
	exit(1);
}

$sql="SELECT * FROM `contest` WHERE `contest_id`='$cid' AND `start_time`<NOW()";
if(!isset($_SESSION['administrator']))
{
	$sql .= " AND ( `duration`='0' or `end_time`<NOW() )" ;
}
$result=mysql_query($sql);
$num=mysql_num_rows($result);
if ($num==0){
	echo "Not Started or will be available after the contest!";
	require_once("oj-footer.php");
	exit();
}
mysql_free_result($result);

$sql="SELECT count(`num`) FROM `contest_problem` WHERE `contest_id`='$cid'";
$result=mysql_query($sql);
$row=mysql_fetch_array($result);
$pid_cnt=intval($row[0]);
mysql_free_result($result);

$sql="SELECT `result`,`num`,`language` FROM `solution` WHERE `contest_id`='$cid' and num>=0"; 
$result=mysql_query($sql);
$R=array();
while ($row=mysql_fetch_object($result)){
	$res=intval($row->result)-4;
	if ($res<0) $res=8;
	$num=intval($row->num);
	$lag=intval($row->language);
	if(!isset($R[$num][$res]))
		$R[$num][$res]=1;
	else
		$R[$num][$res]++;
	if(!isset($R[$num][$lag+11]))
		$R[$num][$lag+11]=1;
	else
		$R[$num][$lag+11]++;
	if(!isset($R[$pid_cnt][$res]))
		$R[$pid_cnt][$res]=1;
	else
		$R[$pid_cnt][$res]++;
	if(!isset($R[$pid_cnt][$lag+11]))
		$R[$pid_cnt][$lag+11]=1;
	else
		$R[$pid_cnt][$lag+11]++;
	if(!isset($R[$num][9]))
		$R[$num][9]=1;
	else
		$R[$num][9]++;
	if(!isset($R[$pid_cnt][9]))
		$R[$pid_cnt][9]=1;
	else
		$R[$pid_cnt][9]++;
}
mysql_free_result($result);
echo '<center><h3>Contest Statistics</h3><table class="pure-table" width=60%>';
echo "<thead><tr align=center><td><td>AC<td>PE<td>WA<td>TLE<td>MLE<td>OLE<td>RE<td>CE<td>Unjudged<td>Total<td><td>C<td>C++<td>Pascal<td>Java<td>Ruby<td>Bash<td>Python<td>PHP<td>Perl<td>C#</td></tr></thead><tbody>";
for ($i=0;$i<$pid_cnt;$i++){
	if(!isset($PID[$i])) $PID[$i]="";
	
	if ($i&1) 
		echo '<tr align=center class="pure-table-odd"><td>';
	else 
		echo "<tr align=center><td>";
	echo "<a href='problem.php?cid=$cid&pid=$i'>$PID[$i]</a>";
	for ($j=0;$j<21;$j++) {
		if(!isset($R[$i][$j])) $R[$i][$j]="";
		echo "<td>".$R[$i][$j];
	}
	echo "</tr>";
}
echo "<tr align=center><td>Total";	
for ($j=0;$j<21;$j++) {
	if(!isset($R[$i][$j])) $R[$i][$j]="";
	echo "<td>".$R[$i][$j];
}
echo "</tr>";
echo "</tbody><table></center>";
?>
<?php require_once("oj-footer.php")?>
